<?php
/**
 * @file
 * Tests ensuring proper behavior of the integration with Token.
 */

require_once drupal_get_path('module', 'versioncontrol_git') . '/tests/VersioncontrolGitTestCase.test';

class VersioncontrolGitTokenTests extends VersioncontrolGitTestCase {

  public static function getInfo() {
    return array(
      'name' => t('EntityQuery integration tests'),
      'description' => t('Tests on EntityQuery integration.'),
      'group' => t('Version Control Git'),
    );
  }

  /**
   * Make sure EntityQuery can access git specific operation properties.
   */
  function testEntityQueryProperties() {
    $repository = $this->versioncontrolCreateRepoFromTestRepo();
    $query = new EntityFieldQuery();
    $result = $query->entityCondition('entity_type', 'versioncontrol_operation')
      ->propertyCondition('repo_id', $repository->repo_id)
      ->propertyCondition('parent_commit', '550a5fb5628901ad6f0a7f933c87ed7570541484')
      ->execute();
    $this->assertTrue(!empty($result['versioncontrol_operation']), t('The entity query produced a result.'));
    $this->assertTrue(count($result['versioncontrol_operation']) == 1, t('There is only one commit found by the entity query.'));
    $vc_op_ids = array_keys($result['versioncontrol_operation']);
    $vc_op_id = array_shift($vc_op_ids);
    $operation = $repository->loadCommit('0f0280aefe4d0c3384cb04a0bbb15c9543f46a69');
    $this->assertTrue($operation instanceof VersioncontrolGitOperation, t('The reference operation was loaded correctly.'));
    $this->assertEqual($operation->vc_op_id, $vc_op_id, t('The expected operation was found using entity query.'));
  }

}
