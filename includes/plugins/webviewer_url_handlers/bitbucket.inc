<?php
/**
 * @file
 * This plugin provides support for bitbucket.
 */

$plugin = array(
  'vcs' => 'git',
  'title' => t('bitbucket URL autogenerator'),
  'url_templates' => array(
    'repository_view' => '%base_url/%repo_name/browse%branch',
    // @todo The log for a file is visible after clicking "History" from a file
    // view URL, but it seems that Bitbucket does not provide a URL for it.
    'file_log_view' => '',
    'directory_log_view' => '',
    'commit_view' => '%base_url/%repo_name/commits/%revision',
    'file_view' => '%base_url/%repo_name/browse/%path%branch',
    'directory_view' => '%base_url/%repo_name/browse/%path%branch',
    // @todo Implement diff URLs?
    // Bitbucket seems to only support diffs between labels, and not arbitrary
    // commits.
    // The bitbucket pattern looks like
    // '%base_url/%repo_name/compare/diff/?targetBranch=refs/heads/foo&sourceBranch=refs/tags/bar#%path'.
    // Version Control items may have more than one label assigned, and given
    // the arguments are VersioncontrolItem objects on
    // VersioncontrolWebviewerUrlHandlerInterface::getDiffUrl(), this cannot be
    // implemented without changing the interface itself.
    'diff' => '',
  ),
  'handler' => array(
    'class' => 'VersioncontrolRepositoryUrlHandlerBitbucket',
    'file' => 'VersioncontrolRepositoryUrlHandlerBitbucket.inc',
    'path' => drupal_get_path('module', 'versioncontrol_git') . '/includes/plugins/webviewer_url_handlers',
    'parent' => 'none',
  ),
);
