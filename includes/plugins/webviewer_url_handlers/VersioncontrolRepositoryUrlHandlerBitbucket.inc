<?php
/**
 * @file
 * Repository Url handler class for Bitbucket.
 */

/**
 * Bitbucket webviewer url handler class.
 */
class VersioncontrolRepositoryUrlHandlerBitbucket extends VersioncontrolRepositoryUrlHandler {

  /**
   * Retrieves the right prefix for a given label.
   *
   * @param VersioncontrolBranch|VersioncontrolTag $label
   *   The label.
   *
   * @return string
   *   The related prefix or empty if the label type is unknown.
   */
  public function getLabelPrefix($label) {
    $prefix = '';
    if ($label->type == VERSIONCONTROL_LABEL_BRANCH) {
      $prefix = 'refs/heads';
    }
    elseif ($label->type == VERSIONCONTROL_LABEL_TAG) {
      $prefix = 'refs/tags';
    }
    return $prefix;
  }

  public function getRepositoryViewUrl($label = NULL) {
    $placeholders = array(
      '%repo_name' => $this->repository->name,
      '%branch'   => is_object($label) ? sprintf('?at=%s/%s', $this->getLabelPrefix($label), $label->name) : '',
    );
    return strtr($this->getTemplateUrl('repository_view'), $placeholders);
  }

  public function getItemViewUrl($item, $current_label = NULL) {
    $placeholders = array(
      '%repo_name' => $this->repository->name,
      '%path'     => substr($item->path, 1),
    );

    if (isset($current_label->type)) {
      $label_name = $current_label->name;
      $placeholders['%branch'] = sprintf('?at=%s/%s', $this->getLabelPrefix($current_label), $label_name);
    }
    else {
      $placeholders['%branch'] = '';
    }

    $view_url = $item->isFile()
      ? $this->getTemplateUrl('file_view')
      : $this->getTemplateUrl('directory_view');

    return strtr($view_url, $placeholders);
  }

}
