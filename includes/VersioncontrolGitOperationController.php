<?php

class VersioncontrolGitOperationController extends VersioncontrolOperationController {
  /**
   * Table alias for queries.
   */
  private $alias;

  /**
   * Extend the base query with the git backend's additional data in
   * {versioncontrol_git_operations}.
   *
   * @return SelectQuery
   */
  protected function buildQueryBase($ids, $conditions) {
    $query = parent::buildQueryBase($ids, $conditions);
    $this->alias = $this->addTable($query, 'versioncontrol_git_operations', 'vcgo', 'base.vc_op_id = vcgo.vc_op_id');
    $query->fields($this->alias, drupal_schema_fields_sql('versioncontrol_git_operations'));
    return $query;
  }

  /**
   * {@inheritdoc}
   *
   * Query {versioncontrol_git_operations} for conditions in that table.
   */
  protected function buildQueryConditions(&$query, $ids, $conditions) {
    foreach ($conditions as $type => $value) {
      if (in_array($type, ['author_name', 'committer_name', 'parent_commit', 'merge'])) {
        $this->attachCondition($query, $type, $value, $this->alias);
        unset($conditions[$type]);
      }
    }
    parent::buildQueryConditions($query, $ids, $conditions);
  }
}
