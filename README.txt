= Version Control API -- Git backend =

Provides a git back-end for versioncontrol module.

== Summary ==

This module provides an implementation of the Version Control API that makes it
possible to use the http://git-scm.com[Git] version control system.

It mainly calls git binary and parses the output to get the information.

Any bug reports or feature requests concerning the Git backend in general
should be submitted to the relevant issue queue:
https://www.drupal.org/project/issues/versioncontrol_git.

If you know that the functionality is (or should be) provided by the
Version Control API (and not by the Git backend), please submit an issue there:
https://www.drupal.org/project/issues/versioncontrol.

== Install ==

Install as usual, see http://drupal.org/node/70151 for further information.

If you've problems with the module, please check that your server is running
git 1.7.0.5 or later.

== Credits ==

Original version for Drupal 5 by https://www.drupal.org/u/boombatower[Jimmy
Berry].
A good amount of code was taken from the CVS backend module, its author, 
https://www.drupal.org/u/jpetso[Jakob Petsovits], among others, deserve a lot of
credits and may also hold copyright for parts of this module.

Initial 6.x-1.x version by https://www.drupal.org/u/marvil07[Marco Villegas],
followed by the parsing logic rewrite by http://drupal.org/user/136353[Cornelius
Riemenschneider].

Rewrite for 6.x-2.x by https://www.drupal.org/u/marvil07[Marco Villegas],
https://www.drupal.org/u/sdboyer[Sam Boyer],
https://www.drupal.org/u/tizzo[Howard Tyson] and
https://www.drupal.org/u/mikey_p[Michael Prasuhn].
Version Control Event implementation by
https://www.drupal.org/u/cvangysel[Christophe Van Gysel].

Port to 7.x-1.x by https://www.drupal.org/u/marvil07[Marco Villegas],
https://www.drupal.org/u/sdboyer[Sam Boyer] and
https://www.drupal.org/u/tizzo[Howard Tyson].

Currently maintained by https://www.drupal.org/u/sdboyer[Sam Boyer] and
https://www.drupal.org/u/marvil07[Marco Villegas].
